interface IWorker {
    id: string;
    name: string;
}

interface FullWorker {
    id: string;
    name: string;
    x: number;
    y: number;
}

export {IWorker, FullWorker}