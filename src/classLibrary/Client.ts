export class HttpClient {
    async ping(endpoint: string, method: string, body: object): Promise<Response> {
        const content: RequestInit = {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        if (method != "GET") {
            content.body = JSON.stringify(body)
        }
        return await fetch(endpoint, content);
    }
}